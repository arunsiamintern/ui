import React from "react";
import "./Footer1.css";
import MediaQuery from "react-responsive";
function Footer1() {
  return (
    <>
      <MediaQuery minDeviceWidth={1200}>
        <div className="footer1">
          <div className="footer1_navbar">
            <span className="footer1_option">About Us</span>
            <span className="footer1_option">Contact Us</span>
            <span className="footer1_option">Terms & condition</span>
            <span className="footer1_option">Privacy Policy</span>
          </div>
        </div>
      </MediaQuery>
      <MediaQuery maxDeviceWidth={600}>
        <div className="footer1">
          <div className="footer1_navbar">
            <span className="footer1_option1">ABOUT US</span>
            <span className="footer1_option1">CONTACT US</span>
            <span className="footer1_option1">TERMS & CONDITION</span>
            <span className="footer1_option1">PRIVACY POLICY</span>
          </div>
        </div>
      </MediaQuery>
    </>
  );
}

export default Footer1;
