import "./App.css";
import MediaQuery from "react-responsive";
function App() {
  return (
    <>
      <MediaQuery minDeviceWidth={1200}>
        <div className="app">
          <span className="app_title">
            <span className="title_question">
              {" "}
              In the Past Three Months, Did You experience Loneliness?
            </span>
            <p className="title_questionlist">view Question List</p>
          </span>
        </div>
        <div className="app">
          <div className="app_b1">
            <button className="button1">Add a question</button>
          </div>

          <div className="app_b2">
            <div className="b2_question">
              In the Past Three Months, Did You experience Loneliness?
            </div>

            <p>
              <button className="b2_button1">View as Data</button>{" "}
              <button className="b2_button2">View as Chart</button>{" "}
            </p>
            <p className="yespercent">63.32%</p>
            <p>Yes</p>
            <p className="nopercent"> 36.68%</p>
            <p>No</p>
          </div>

          <div className="app_b3">
            <button className="button1">Add a question</button>
          </div>

          <div className="app_b4">
            <button className="button1">Download Data</button>
          </div>
        </div>
      </MediaQuery>
      <MediaQuery maxDeviceWidth={600}>
        <div className="app1">
          <div className="app1_b2">
            <div className="b2_question">
              In the Past Three Months, Did You experience Loneliness?
            </div>
            <p className="title_questionlist">view Question List</p>
            <p>
              <button className="b2_button1">View as Data</button>{" "}
              <button className="b2_button2">View as Chart</button>{" "}
            </p>
            <p className="yespercent1">63.32%</p>

            <p>Yes</p>
            <p className="nopercent1"> 36.68%</p>
            <p>No</p>
            <p>
              <button className="b2_button1">Add a question</button>{" "}
              <button className="b2_button1">Download Data</button>
            </p>
          </div>
        </div>
      </MediaQuery>
    </>
  );
}

export default App;
