import React from "react";
import "./Footer.css";
import AdbIcon from "@material-ui/icons/Adb";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import GTranslateIcon from "@material-ui/icons/GTranslate";
import PinterestIcon from "@material-ui/icons/Pinterest";
function Footer() {
  return (
    <div className="footer">
      <div className="footer_navbar">
        <span className="footer_option">
          <FacebookIcon />{" "}
        </span>

        <span className="footer_option">
          <GTranslateIcon />{" "}
        </span>
        <span className="footer_option">
          <InstagramIcon />{" "}
        </span>
        <span className="footer_option">
          <PinterestIcon />{" "}
        </span>
        <span className="footer_option">
          <AdbIcon />
        </span>
      </div>
    </div>
  );
}

export default Footer;
