import React from "react";
import "./Nav.css";
import MediaQuery from "react-responsive";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
function Nav() {
  return (
    <>
      <MediaQuery minDeviceWidth={1200}>
        <div className="nav">
          <div className="nav_right">
            <h3>Profile Builder</h3>
          </div>

          <div className="nav_left">
            <button className="button"> Clear Filters </button>
          </div>
        </div>

        <div className="nav">
          <div className="nav_right">
            <h5>Region</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Country</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Age</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Gender</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Religion</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Committed in Faith</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>

        <div className="nav">
          <div className="nav_right">
            <h5>Reading Religious scriptures</h5>
          </div>

          <div className="nav_left">
            <h3>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h3>
          </div>
        </div>
        <div className="nav">
          <div className="nav_right">
            <h5>Religious Event Attendance</h5>
          </div>

          <div className="nav_left">
            <h2>
              {" "}
              <KeyboardArrowDownIcon />{" "}
            </h2>
          </div>
        </div>
      </MediaQuery>
    </>
  );
}

export default Nav;
