import React from "react";
import "./Header.css";
import MediaQuery from "react-responsive";
import MenuIcon from "@material-ui/icons/Menu";

function Header() {
  return (
    <div>
      <MediaQuery minDeviceWidth={1200}>
        <div className="header">
          <div className="header_navbar">
            <span className="header_option">Home</span>
            <span className="header_option">Data explorer</span>
            <span className="header_option">Reports & Resources</span>
            <span className="header_option">Do You know Gen Z?</span>
            <button className="navbar_button">Share</button>
          </div>
        </div>
      </MediaQuery>

      <MediaQuery maxDeviceWidth={600}>
        <div className="header1">
          <div className="header1_navbar">
            <span className="header1_option1">
              {" "}
              <MenuIcon />{" "}
            </span>
            <span className="header1_option">HOME</span>
            <span className="header1_option">MOBILE</span>
            <span className="header1_option">REPORTS </span>
            <span className="header1_option">GEN Z ?</span>
            <button className="navbar1_button">SHARE</button>
          </div>
        </div>
      </MediaQuery>
    </div>
  );
}

export default Header;
