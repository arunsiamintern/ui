import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Header from "./Header";
import Footer from "./Footer";
import Footer1 from "./Footer1";
import Nav from "./Nav";
import MediaQuery from "react-responsive";
ReactDOM.render(
  <React.StrictMode>
    <MediaQuery minDeviceWidth={1200}>
      <Header />
      <div className="content">
        <div className="content_nav">
          <Nav />
        </div>
        <div className="content_app">
          <App />
        </div>
      </div>
      <Footer />
      <Footer1 />
    </MediaQuery>
    <MediaQuery maxDeviceWidth={600}>
      <Header />
      <div className="content1">
        <div className="content1_app">
          <App />
        </div>
      </div>
      <Footer />
      <Footer1 />
    </MediaQuery>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
